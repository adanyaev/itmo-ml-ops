import json
import xmltodict
import os
import os.path as osp

import click
import tqdm
import cv2


@click.command()
@click.argument("imgs_root", type=click.Path(exists=True, file_okay=False))
@click.argument(
    "load_anns_path", type=click.Path(exists=True, file_okay=False)
)
@click.argument(
    "save_anns_path", type=click.Path(exists=False, file_okay=False)
)
def convert_anns(imgs_root, load_anns_path, save_anns_path):

    if not osp.exists(save_anns_path):
        os.makedirs(save_anns_path)

    dataset_names = list(
        filter(
            lambda x: os.path.isdir(os.path.join(imgs_root, x)),
            os.listdir(imgs_root),
        )
    )

    for name in tqdm.tqdm(dataset_names):
        xml_path = os.path.join(load_anns_path, f"{name}.xml")
        with open(xml_path, "r") as xml_file:
            data_dict = xmltodict.parse(xml_file.read())

        images_path = os.path.join(imgs_root, name)
        images = []
        img_ids = []
        annotations = []

        images_paths = os.listdir(images_path)
        images_paths = sorted(
            images_paths, key=lambda x: int(x.split(".")[-2])
        )
        for i in images_paths:
            img_data = cv2.imread(osp.join(images_path, i))
            img_id = int(i.split(".")[-2])
            img_ids.append(img_id)
            img_ann = {
                "id": img_id,
                "file_name": i,
                "height": img_data.shape[0],
                "width": img_data.shape[1],
            }
            images.append(img_ann)

        ann_counter = 0
        for car_track in data_dict["Data"]["Vehicle"]:
            for frame in car_track["Frame"]:
                if not isinstance(frame, dict):
                    continue

                img_id = int(frame["@id"]) - 1
                if img_id not in img_ids:
                    continue

                polygon = [
                    int(frame["PlateLocation"]["TopLeft"]["@x"]),
                    int(frame["PlateLocation"]["TopLeft"]["@y"]),
                    int(frame["PlateLocation"]["TopRight"]["@x"]),
                    int(frame["PlateLocation"]["TopRight"]["@y"]),
                    int(frame["PlateLocation"]["BottomLeft"]["@x"]),
                    int(frame["PlateLocation"]["BottomLeft"]["@y"]),
                    int(frame["PlateLocation"]["BottomRight"]["@x"]),
                    int(frame["PlateLocation"]["BottomRight"]["@y"]),
                ]
                polygon_formatted = [
                    polygon[0],
                    polygon[1],
                    polygon[2],
                    polygon[3],
                    polygon[6],
                    polygon[7],
                    polygon[4],
                    polygon[5],
                ]
                car_ann = {
                    "id": ann_counter,
                    "car_id": int(car_track["@id"]),
                    "image_id": img_id,
                    "plate_text": car_track["FinalPlateText"]["@value"],
                    "plate_type": car_track["FinalPlateType"]["@value"],
                    "segmentation": [polygon],
                    "category_id": 0,
                }
                xmin = polygon[0] if polygon[0] < polygon[4] else polygon[4]
                xmax = polygon[2] if polygon[2] > polygon[6] else polygon[6]
                ymin = polygon[1] if polygon[1] < polygon[3] else polygon[3]
                ymax = polygon[5] if polygon[5] > polygon[7] else polygon[7]
                car_ann["bbox"] = [xmin, ymin, xmax - xmin, ymax - ymin]
                car_ann["polygon"] = polygon_formatted
                car_ann["area"] = car_ann["bbox"][2] * car_ann["bbox"][3]
                annotations.append(car_ann)
                ann_counter += 1

        categories = [
            {"supercategory": "plate", "id": 0, "name": "license_plate"}
        ]

        final_data = {}

        final_data["info"] = {
            "description": "Russian LPR dataset",
            "url": "https://rb.gy/fxn5l",
            "version": "1.0",
            "year": 2023,
            "date_created": "2023/09/26",
        }
        final_data["categories"] = categories
        final_data["images"] = images
        final_data["annotations"] = annotations

        with open(os.path.join(save_anns_path, f"{name}.json"), "w") as f:
            json.dump(final_data, f)


if __name__ == "__main__":
    convert_anns()
