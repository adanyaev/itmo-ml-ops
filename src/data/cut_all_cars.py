import os
import click
import pickle
import json
import cv2
import tqdm


def load_datasets(global_img_root):
    dataset_names = list(
        filter(
            lambda x: os.path.isdir(os.path.join(global_img_root, x)),
            os.listdir(global_img_root),
        )
    )
    return dataset_names


def load_annotations(global_car_dets_path, global_plate_anns_path, name):
    with open(os.path.join(global_car_dets_path, f"{name}.pkl"), "rb") as f:
        car_dets = pickle.load(f)
    with open(os.path.join(global_plate_anns_path, f"{name}.json"), "r") as f:
        annos = json.load(f)
    return car_dets, annos


def match_car_plate(car_bbox, plate_bbox):
    if plate_bbox[0] > car_bbox[0] and plate_bbox[1] > car_bbox[1]:
        if plate_bbox[2] < car_bbox[2] and plate_bbox[3] < car_bbox[3]:
            return True
    return False


def process_images(
    name,
    annos,
    img_root,
    save_img_root,
    car_dets,
    global_save_anns_path,
    global_save_img_root,
):
    image_new_anns = []
    plate_new_anns = []
    img_anns_counter = 0
    plate_anns_counter = 0

    for img_ann in tqdm.tqdm(
        annos["images"], position=1, leave=False, desc=name
    ):
        plate_anns = [
            i for i in annos["annotations"] if i["image_id"] == img_ann["id"]
        ]
        plate_bboxes = [
            [
                i["bbox"][0],
                i["bbox"][1],
                i["bbox"][0] + i["bbox"][2],
                i["bbox"][1] + i["bbox"][3],
            ]
            for i in plate_anns
        ]
        car_bboxes = [
            i["pred_instances"]["bboxes"].tolist()
            for i in car_dets
            if i["img_id"] == img_ann["id"]
        ]

        if car_bboxes and len(car_bboxes[0]) == 0:
            continue

        if car_bboxes and isinstance(car_bboxes[0][0], list):
            car_bboxes = car_bboxes[0]

        if car_bboxes and len(car_bboxes[0]) == 0:
            continue

        single_img_anns_counter = 0

        for car_bbox in car_bboxes:
            for plate_bbox_idx in range(len(plate_bboxes)):
                plate_bbox = plate_bboxes[plate_bbox_idx]
                if match_car_plate(car_bbox, plate_bbox):
                    img = cv2.imread(
                        os.path.join(img_root, img_ann["file_name"])
                    )
                    img_crop = img[
                        int(car_bbox[1]) : int(car_bbox[3]),
                        int(car_bbox[0]) : int(car_bbox[2]),
                        :,
                    ]
                    cv2.imwrite(
                        os.path.join(
                            save_img_root,
                            img_ann["file_name"][:-4]
                            + "_"
                            + str(single_img_anns_counter)
                            + ".bmp",
                        ),
                        img_crop,
                    )
                    new_plate_bbox = [
                        plate_bbox[0] - car_bbox[0],
                        plate_bbox[1] - car_bbox[1],
                        plate_bbox[2] - plate_bbox[0],
                        plate_bbox[3] - plate_bbox[1],
                    ]
                    image_new_anns.append(
                        {
                            "id": img_anns_counter,
                            "file_name": img_ann["file_name"][:-4]
                            + "_"
                            + str(single_img_anns_counter)
                            + ".bmp",
                            "height": img_crop.shape[0],
                            "width": img_crop.shape[1],
                        }
                    )
                    plate_new_anns.append(
                        {
                            "id": plate_anns_counter,
                            "car_id": plate_anns[plate_bbox_idx]["car_id"],
                            "image_id": img_anns_counter,
                            "plate_text": plate_anns[plate_bbox_idx][
                                "plate_text"
                            ],
                            "plate_type": plate_anns[plate_bbox_idx][
                                "plate_type"
                            ],
                            "category_id": 0,
                            "bbox": new_plate_bbox,
                            "area": plate_anns[plate_bbox_idx]["area"],
                        }
                    )
                    img_anns_counter += 1
                    plate_anns_counter += 1
                    single_img_anns_counter += 1

    return image_new_anns, plate_new_anns


def create_new_annotations(annos, image_new_anns, plate_new_anns):
    new_anns = {
        "info": {
            "description": "Russian LPR dataset cropped cars",
            "url": "https://rb.gy/fxn5l",
            "version": "1.0",
            "year": 2023,
            "date_created": "2023/10/01",
        }
    }
    new_anns["categories"] = annos["categories"]
    new_anns["images"] = image_new_anns
    new_anns["annotations"] = plate_new_anns

    return new_anns


@click.command()
@click.argument(
    "global_img_root", type=click.Path(exists=True, file_okay=False)
)
@click.argument(
    "global_car_dets_path", type=click.Path(exists=True, file_okay=False)
)
@click.argument(
    "global_plate_anns_path", type=click.Path(exists=True, file_okay=False)
)
@click.argument(
    "global_save_img_root", type=click.Path(exists=False, file_okay=False)
)
@click.argument(
    "global_save_anns_path", type=click.Path(exists=False, file_okay=False)
)
def cut_cars(
    global_img_root,
    global_car_dets_path,
    global_plate_anns_path,
    global_save_img_root,
    global_save_anns_path,
):
    dataset_names = load_datasets(global_img_root)

    dataset_names = list(
        filter(
            lambda x: os.path.isdir(os.path.join(global_img_root, x)),
            os.listdir(global_img_root),
        )
    )

    click.echo(dataset_names)

    for name in tqdm.tqdm(dataset_names, position=0):

        img_root = os.path.join(global_img_root, name)
        save_img_root = os.path.join(global_save_img_root, name)
        save_anns_file = os.path.join(global_save_anns_path, f"{name}.json")

        car_dets, annos = load_annotations(
            global_car_dets_path, global_plate_anns_path, name
        )

        if not os.path.exists(save_img_root):
            os.makedirs(save_img_root)

        if not os.path.exists(global_save_anns_path):
            os.mkdir(global_save_anns_path)

        image_new_anns, plate_new_anns = process_images(
            name,
            annos,
            img_root,
            save_img_root,
            car_dets,
            global_save_anns_path,
            global_save_img_root,
        )

        new_anns = create_new_annotations(
            annos, image_new_anns, plate_new_anns
        )

        with open(save_anns_file, "w") as f:
            json.dump(new_anns, f)


if __name__ == "__main__":
    cut_cars()
