import os
import json
import tqdm
import click


@click.command()
@click.argument("load_anns_dir", type=click.Path(exists=True, file_okay=False))
@click.argument(
    "save_anns_path", type=click.Path(exists=False, file_okay=True)
)
def merge_anns(load_anns_dir, save_anns_path):

    data_prefixes = sorted(
        list(map(lambda x: x[:-5], os.listdir(load_anns_dir)))
    )
    json_files_paths = [
        os.path.join(load_anns_dir, i + ".json") for i in data_prefixes
    ]

    data = {"images": [], "annotations": []}
    img_id_counter = 0
    ann_id_counter = 0
    uniq_car_counter = 0
    assert len(json_files_paths) == len(data_prefixes)
    print("Hello")
    for i in tqdm.tqdm(range(len(json_files_paths))):

        with open(json_files_paths[i], "r") as f:
            data_temp = json.load(f)
        # if 'licenses' not in data:
        #     data['licenses'] = data_temp['licenses']
        if "info" not in data:
            data["info"] = data_temp["info"]
        if "categories" not in data:
            data["categories"] = data_temp["categories"]

        id_map = {}
        local_car_ids = {}

        for img in data_temp["images"]:
            id_map[img["id"]] = img_id_counter
            img["id"] = img_id_counter
            img["file_name"] = data_prefixes[i] + "/" + img["file_name"]
            img_id_counter += 1
            data["images"].append(img)

        for ann in data_temp["annotations"]:
            if ann["car_id"] not in local_car_ids.keys():
                local_car_ids[ann["car_id"]] = uniq_car_counter
                uniq_car_counter += 1
            ann["car_id"] = local_car_ids[ann["car_id"]]
            ann["id"] = ann_id_counter
            ann["image_id"] = id_map[ann["image_id"]]
            ann_id_counter += 1
            data["annotations"].append(ann)

    with open(save_anns_path, "w") as f:
        json.dump(data, f)

    print("success")


if __name__ == "__main__":
    merge_anns()
