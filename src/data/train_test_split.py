import json
import random
import copy
import tqdm
import os

import click


def generate_split_anns(imgs_ids, data, save_path):
    anns = copy.deepcopy(data)
    imgs_counter = 0
    anns_counter = 0
    new_imgs = []
    new_anns = []
    for i in tqdm.tqdm(imgs_ids):
        ann = copy.deepcopy(
            list(filter(lambda x: x["image_id"] == i, anns["annotations"]))
        )
        if not ann:
            print("error ann")
            continue
        img = copy.deepcopy(
            list(filter(lambda x: x["id"] == i, anns["images"]))
        )
        if not img:
            print("error img")
            continue
        ann = ann[0]
        img = img[0]
        ann["id"] = anns_counter
        ann["image_id"] = imgs_counter
        ann["iscrowd"] = False
        img["id"] = imgs_counter
        new_imgs.append(img)
        new_anns.append(ann)
        imgs_counter += 1
        anns_counter += 1
    anns["images"] = new_imgs
    anns["annotations"] = new_anns

    with open(save_path, "w") as f:
        json.dump(anns, f)


@click.command()
@click.argument(
    "global_anns_path", type=click.Path(exists=True, file_okay=True)
)
@click.argument(
    "save_anns_dir", type=click.Path(exists=False, file_okay=False)
)
def split_anns(global_anns_path, save_anns_dir):

    with open(global_anns_path) as f:
        data = json.load(f)

    if not os.path.exists(save_anns_dir):
        os.makedirs(save_anns_dir)

    imgs_ids = {}
    for ann in data["annotations"]:
        if ann["car_id"] not in imgs_ids:
            imgs_ids[ann["car_id"]] = [ann["image_id"]]
        else:
            imgs_ids[ann["car_id"]].append(ann["image_id"])

    car_ids = list(imgs_ids.keys())
    random.shuffle(car_ids)

    val_samples_num = int(len(car_ids) * 0.05)
    test_samples_num = int(len(car_ids) * 0.1)

    test_car_ids = car_ids[:test_samples_num]
    val_car_ids = car_ids[
        test_samples_num : test_samples_num + val_samples_num
    ]
    training_car_ids = car_ids[test_samples_num + val_samples_num :]
    training_imgs_ids = [j for i in training_car_ids for j in imgs_ids[i]]
    val_imgs_ids = [j for i in val_car_ids for j in imgs_ids[i]]
    test_imgs_ids = [j for i in test_car_ids for j in imgs_ids[i]]

    generate_split_anns(
        training_imgs_ids,
        data,
        os.path.join(save_anns_dir, "train_anns.json"),
    )
    generate_split_anns(
        val_imgs_ids, data, os.path.join(save_anns_dir, "val_anns.json")
    )
    generate_split_anns(
        test_imgs_ids, data, os.path.join(save_anns_dir, "test_anns.json")
    )

    print("success")


if __name__ == "__main__":
    split_anns()
