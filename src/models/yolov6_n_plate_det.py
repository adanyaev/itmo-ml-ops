default_scope = "mmyolo"

file_client_args = None

env_cfg = dict(
    cudnn_benchmark=True,
    mp_cfg=dict(mp_start_method="fork", opencv_num_threads=0),
    dist_cfg=dict(backend="nccl"),
)


log_processor = dict(type="LogProcessor", window_size=50, by_epoch=True)

log_level = "INFO"
backend_args = None


data_root = "data/processed/"


train_ann_file = "anns_splitted/train_anns.json"

val_ann_file = "anns_splitted/val_anns.json"

test_ann_file = "anns_splitted/test_anns.json"

train_data_prefix = "imgs/"
val_data_prefix = "imgs/"
test_data_prefix = "imgs/"

work_dir = "models/logs"

load_from = "models/yolov6_n_coco.pth"

classes = ("license_plate",)

num_classes = len(classes)  # Number of classes for classification

metainfo = dict(classes=classes, palette=[(255, 0, 0)])


train_batch_size_per_gpu = 32
# Worker to pre-fetch data for each single GPU during training
train_num_workers = 4
# persistent_workers must be False if num_workers is 0
persistent_workers = True

# -----train val related-----
# Base learning rate for optim_wrapper
base_lr = 0.001 / 8
max_epochs = 10  # Maximum training epochs
num_last_epochs = 1  # Last epoch number to switch training pipeline

# ======================= Possible modified parameters =======================
# -----data related-----
img_scale = (640, 640)  # width, height
# Dataset type, this will be used to define the dataset
dataset_type = "YOLOv5CocoDataset"
# Batch size of a single GPU during validation
val_batch_size_per_gpu = 8
# Worker to pre-fetch data for each single GPU during validation
val_num_workers = 2

batch_shapes_cfg = None

# -----model related-----
# The scaling factor that controls the depth of the network structure
deepen_factor = 0.33
# The scaling factor that controls the width of the network structure
widen_factor = 0.25

# -----train val related-----
affine_scale = 0.5  # YOLOv5RandomAffine scaling ratio
lr_factor = 0.02  # Learning rate scaling factor
weight_decay = 0.0005
# Save model checkpoint and validation intervals
save_epoch_intervals = 1
# The maximum checkpoints to keep.
max_keep_ckpts = 1
# Single-scale training is recommended to
# be turned on, which can speed up training.

# ============================== Unmodified in most cases ===================
model = dict(
    type="YOLODetector",
    data_preprocessor=dict(
        type="YOLOv5DetDataPreprocessor",
        mean=[0.0, 0.0, 0.0],
        std=[255.0, 255.0, 255.0],
        bgr_to_rgb=True,
    ),
    backbone=dict(
        type="YOLOv6EfficientRep",
        deepen_factor=deepen_factor,
        widen_factor=widen_factor,
        norm_cfg=dict(type="BN", momentum=0.03, eps=0.001),
        act_cfg=dict(type="ReLU", inplace=True),
    ),
    neck=dict(
        type="YOLOv6RepPAFPN",
        deepen_factor=deepen_factor,
        widen_factor=widen_factor,
        in_channels=[256, 512, 1024],
        out_channels=[128, 256, 512],
        num_csp_blocks=12,
        norm_cfg=dict(type="BN", momentum=0.03, eps=0.001),
        act_cfg=dict(type="ReLU", inplace=True),
    ),
    bbox_head=dict(
        type="YOLOv6Head",
        head_module=dict(
            type="YOLOv6HeadModule",
            num_classes=num_classes,
            in_channels=[128, 256, 512],
            widen_factor=widen_factor,
            norm_cfg=dict(type="BN", momentum=0.03, eps=0.001),
            act_cfg=dict(type="SiLU", inplace=True),
            featmap_strides=[8, 16, 32],
        ),
        loss_bbox=dict(
            type="IoULoss",
            iou_mode="siou",
            bbox_format="xyxy",
            reduction="mean",
            loss_weight=2.5,
            return_iou=False,
        ),
    ),
    train_cfg=dict(
        initial_epoch=4,
        initial_assigner=dict(
            type="BatchATSSAssigner",
            num_classes=num_classes,
            topk=9,
            iou_calculator=dict(type="mmdet.BboxOverlaps2D"),
        ),
        assigner=dict(
            type="BatchTaskAlignedAssigner",
            num_classes=num_classes,
            topk=13,
            alpha=1,
            beta=6,
        ),
    ),
    test_cfg=dict(
        multi_label=False,
        # The number of boxes before NMS
        nms_pre=300,
        score_thr=0.001,  # Threshold to filter out boxes.
        nms=dict(type="nms", iou_threshold=0.3),  # NMS type and threshold
        max_per_img=5,
    ),
)

# The training pipeline of YOLOv6 is basically the same as YOLOv5.
# The difference is that Mosaic and RandomAffine will be closed in the last 15 epochs. # noqa
pre_transform = [
    dict(type="LoadImageFromFile", backend_args=backend_args),
    dict(type="LoadAnnotations", with_bbox=True),
]

train_pipeline = [
    *pre_transform,
    dict(
        type="Mosaic",
        img_scale=img_scale,
        pad_val=114.0,
        pre_transform=pre_transform,
    ),
    dict(
        type="YOLOv5RandomAffine",
        max_rotate_degree=0.0,
        max_translate_ratio=0.1,
        scaling_ratio_range=(1 - affine_scale, 1 + affine_scale),
        # img_scale is (width, height)
        border=(-img_scale[0] // 2, -img_scale[1] // 2),
        border_val=(114, 114, 114),
        max_shear_degree=0.0,
    ),
    # dict(type='YOLOv5HSVRandomAug'),
    dict(type="mmdet.RandomFlip", prob=0.5),
    dict(
        type="mmdet.PackDetInputs",
        meta_keys=(
            "img_id",
            "img_path",
            "ori_shape",
            "img_shape",
            "flip",
            "flip_direction",
        ),
    ),
]

train_pipeline_stage2 = [
    *pre_transform,
    dict(type="YOLOv5KeepRatioResize", scale=img_scale),
    dict(
        type="LetterResize",
        scale=img_scale,
        allow_scale_up=True,
        pad_val=dict(img=114),
    ),
    dict(
        type="YOLOv5RandomAffine",
        max_rotate_degree=0.0,
        max_translate_ratio=0.1,
        scaling_ratio_range=(1 - affine_scale, 1 + affine_scale),
        max_shear_degree=0.0,
    ),
    # dict(type='YOLOv5HSVRandomAug'),
    dict(type="mmdet.RandomFlip", prob=0.5),
    dict(
        type="mmdet.PackDetInputs",
        meta_keys=(
            "img_id",
            "img_path",
            "ori_shape",
            "img_shape",
            "flip",
            "flip_direction",
        ),
    ),
]

train_dataloader = dict(
    batch_size=train_batch_size_per_gpu,
    num_workers=train_num_workers,
    collate_fn=dict(type="yolov5_collate"),
    persistent_workers=persistent_workers,
    pin_memory=True,
    sampler=dict(type="DefaultSampler", shuffle=True),
    dataset=dict(
        type=dataset_type,
        metainfo=metainfo,
        data_root=data_root,
        ann_file=train_ann_file,
        data_prefix=dict(img=train_data_prefix),
        filter_cfg=dict(filter_empty_gt=False, min_size=32),
        pipeline=train_pipeline,
    ),
)

test_pipeline = [
    dict(type="LoadImageFromFile", backend_args=backend_args),
    dict(type="YOLOv5KeepRatioResize", scale=img_scale),
    dict(
        type="LetterResize",
        scale=img_scale,
        allow_scale_up=False,
        pad_val=dict(img=114),
    ),
    dict(type="LoadAnnotations", with_bbox=True, _scope_="mmdet"),
    dict(
        type="mmdet.PackDetInputs",
        meta_keys=(
            "img_id",
            "img_path",
            "ori_shape",
            "img_shape",
            "scale_factor",
            "pad_param",
        ),
    ),
]

val_dataloader = dict(
    batch_size=val_batch_size_per_gpu,
    num_workers=val_num_workers,
    persistent_workers=persistent_workers,
    pin_memory=True,
    drop_last=False,
    sampler=dict(type="DefaultSampler", shuffle=False),
    dataset=dict(
        type=dataset_type,
        data_root=data_root,
        metainfo=metainfo,
        # test_mode=True,
        ann_file=val_ann_file,
        data_prefix=dict(img=val_data_prefix),
        pipeline=test_pipeline,
        batch_shapes_cfg=batch_shapes_cfg,
    ),
)

test_dataloader = dict(
    batch_size=val_batch_size_per_gpu,
    num_workers=val_num_workers,
    persistent_workers=persistent_workers,
    pin_memory=True,
    drop_last=False,
    sampler=dict(type="DefaultSampler", shuffle=False),
    dataset=dict(
        type=dataset_type,
        data_root=data_root,
        metainfo=metainfo,
        # test_mode=True,
        data_prefix=dict(img=test_data_prefix),
        ann_file=test_ann_file,
        pipeline=test_pipeline,
        batch_shapes_cfg=batch_shapes_cfg,
    ),
)

# Optimizer and learning rate scheduler of YOLOv6 are basically the same as YOLOv5. # noqa
# The difference is that the scheduler_type of YOLOv6 is cosine.
optim_wrapper = dict(
    type="OptimWrapper",
    optimizer=dict(
        type="SGD",
        lr=base_lr,
        momentum=0.937,
        weight_decay=weight_decay,
        nesterov=True,
        batch_size_per_gpu=train_batch_size_per_gpu,
    ),
    constructor="YOLOv5OptimizerConstructor",
)

default_hooks = dict(
    timer=dict(type="IterTimerHook"),
    logger=dict(type="LoggerHook", interval=50),
    sampler_seed=dict(type="DistSamplerSeedHook"),
    visualization=dict(type="mmdet.DetVisualizationHook"),
    param_scheduler=dict(
        type="YOLOv5ParamSchedulerHook",
        scheduler_type="cosine",
        lr_factor=lr_factor,
        max_epochs=max_epochs,
        warmup_mim_iter=10,
    ),
    checkpoint=dict(
        type="CheckpointHook",
        interval=save_epoch_intervals,
        max_keep_ckpts=max_keep_ckpts,
        save_best="auto",
    ),
)

custom_hooks = [
    dict(
        type="EMAHook",
        ema_type="ExpMomentumEMA",
        momentum=0.0001,
        update_buffers=True,
        strict_load=False,
        priority=49,
    ),
    dict(
        type="mmdet.PipelineSwitchHook",
        switch_epoch=max_epochs - num_last_epochs,
        switch_pipeline=train_pipeline_stage2,
    ),
]

val_evaluator = dict(
    type="mmdet.CocoMetric",
    proposal_nums=(100, 1, 10),
    ann_file=data_root + val_ann_file,
    metric="bbox",
)

test_evaluator = dict(
    type="mmdet.CocoMetric",
    proposal_nums=(100, 1, 10),
    ann_file=data_root + test_ann_file,
    metric="bbox",
)

train_cfg = dict(
    type="EpochBasedTrainLoop",
    max_epochs=max_epochs,
    val_interval=save_epoch_intervals,
    dynamic_intervals=[(max_epochs - num_last_epochs, 1)],
)
val_cfg = dict(type="ValLoop")
test_cfg = dict(type="TestLoop")


visualizer = dict(
    type="mmdet.DetLocalVisualizer",
    name="visualizer",
    vis_backends=[
        dict(type="LocalVisBackend"),
        dict(
            type="MLflowVisBackend",
            save_dir="./models/logs/mlflow_logs",
            exp_name="car_plate_det_v3",
            run_name="v1",
            tracking_uri="http://91.107.123.193:5000/",
        ),
    ],
)
