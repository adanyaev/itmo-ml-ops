# import json

# import fiftyone.utils.coco as fouc
import fiftyone as fo


coco_dataset = fo.Dataset.from_dir(
    dataset_type=fo.types.COCODetectionDataset,
    data_path="/home/artem/work/programming/itmo_cv_project/data/\
        road_signs_data/merged_data/images",
    labels_path="/home/artem/work/programming/itmo_cv_project/data/\
        road_signs_data/merged_data/anns/v1/val_anns.json",
    label_field="ground_truth",
    include_id=True,
)

print(coco_dataset.default_classes)  # ['airplane', 'apple', ...]

print(coco_dataset)

classes = coco_dataset.default_classes

session = fo.launch_app()
session.wait()
