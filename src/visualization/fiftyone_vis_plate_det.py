import json
import os.path as osp

# import fiftyone.utils.coco as fouc
import fiftyone as fo
import tqdm


coco_dataset = fo.Dataset.from_dir(
    dataset_type=fo.types.COCODetectionDataset,
    data_path="/home/artem/work/programming/itmo_cv_project/data/\
        imgs/car_crops",
    labels_path="/home/artem/work/programming/itmo_cv_project/data/\
        annotations/car_crops_anns_v2/v1/test_anns.json",
    label_field="ground_truth",
    include_id=True,
)

print(coco_dataset.default_classes)

print(coco_dataset)

with open(
    "/home/artem/work/programming/itmo_cv_project/data\
        /annotations/car_crops_anns_v2/v1/test_anns.json"
) as f:
    annos = json.load(f)

with open(
    "/home/artem/work/programming/itmo_cv_project/mmyolo/\
        training_logs/plates_det/v7/test_preds.bbox.json"
) as f:
    preds = json.load(f)


preds = [i for i in preds if i["score"] > 0.3]

classes = coco_dataset.default_classes

imgs_root = "/home/artem/work/programming/itmo_cv_project/data/imgs/car_crops"

for i, sample in tqdm.tqdm(enumerate(coco_dataset)):
    ann_filter = filter(
        lambda x: osp.join(imgs_root, x["file_name"]) == sample["filepath"],
        annos["images"],
    )
    ann = list(ann_filter)[0]
    pred = list(filter(lambda x: x["image_id"] == ann["id"], preds))
    dets = []
    for p in pred:

        bbox = [
            p["bbox"][0] / ann["width"],
            p["bbox"][1] / ann["height"],
            p["bbox"][2] / ann["width"],
            p["bbox"][3] / ann["height"],
        ]
        det = fo.Detection(
            label="license_plate", bounding_box=bbox, confidence=p["score"]
        )
        dets.append(det)
    sample["predictions"] = fo.Detections(detections=dets)
    sample.save()

results = coco_dataset.evaluate_detections(
    "predictions",
    gt_field="ground_truth_detections",
    method="coco",
    eval_key="eval",
)


session = fo.launch_app()
session.wait()
