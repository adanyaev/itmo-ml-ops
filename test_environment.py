import sys

R_P = "python"


def main():
    system_major = sys.version_info.major
    if R_P == "python":
        required_major = 2
    elif R_P == "python3":
        required_major = 3
    else:
        raise ValueError("Unrecognized python interpreter: {}".format(R_P))

    if system_major != required_major:
        raise TypeError(
            "This project requires Python {}. Found: Python {}".format(
                required_major, sys.version
            )
        )
    else:
        print(">>> Development environment passes all tests!")


if __name__ == "__main__":
    main()
